/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aomsinjook.midterm;

/**
 *
 * @author cherz__n
 */
import java.io.Serializable;
public class Product implements Serializable{

    private String ID;
    private String Name;
    private String Brand;
    private int Price;
    private String Amount;

    public Product(String ID, String Name, String Brand, int Price, String Amount) {
        this.ID = ID;
        this.Name = Name;
        this.Brand = Brand;
        this.Price = Price;
        this.Amount = Amount;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String Brand) {
        this.Brand = Brand;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int Price) {
        this.Price = Price;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String Amount) {
        this.Amount = Amount;
    }

    @Override
    public String toString() {
        return "ID :" + " " + ID + " Name :" + " " + Name + " Brand :" + " " + Brand
                + " Price :" + " " + Price + " Amount :" + " " + Amount;
    }

}
