/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aomsinjook.midterm;

/**
 *
 * @author cherz__n
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Service {

    private static ArrayList<Product> ProducList = null;

    static {
        ProducList = new ArrayList<>();
        lode();
        //ProducList.add(new product("2", "Melon", "Aomsin", 20,"0"));
        //ProducList.add(new product("3", "Mango", "Kfc", 30,"0"));
        //ProducList.add(new product("4", "Orane", "Aomsin", 10,"0"));
        //ProducList.add(new product("5", "Pice", "Aomsin", 35,"0"));

    }

    public static boolean Addproduct(Product product) {
        ProducList.add(product);
        save();
        return true;

    }
//delete

    public static boolean Deleteproduct(Product product) {
        ProducList.remove(product);
        save();
        return true;

    }
//delete

    public static boolean Deleteproduct(int index) {
        ProducList.remove(index);
        save();
        return true;

    }public static boolean Deleteproduct() {
        ProducList.clear();
        save();
        return true;

    }

    //read
    public static ArrayList<Product> gettest() {
        return ProducList;

    }

    public static Product getproductList(int index) {
        return ProducList.get(index);

    }

    //update
    public static boolean updateProduct(int index, Product product) {
        ProducList.set(index, product);
        save();
        return true;
    }

    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("user.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(ProducList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void lode() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("user.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            ProducList = (ArrayList<Product>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
